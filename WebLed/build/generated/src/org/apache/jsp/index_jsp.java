package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("        <style>\n");
      out.write("            table{\n");
      out.write("                position: absolute;\n");
      out.write("                top: 50%;\n");
      out.write("                left: 50%;\n");
      out.write("                transform: translate(-50%, -50%);\n");
      out.write("                text-align: center;\n");
      out.write("            }\n");
      out.write("            td{\n");
      out.write("                margin: 0;\n");
      out.write("                padding: 0;\n");
      out.write("                width: 25px;\n");
      out.write("                height: 25px;\n");
      out.write("            }\n");
      out.write("            \n");
      out.write("            td span{\n");
      out.write("                width: 10px;\n");
      out.write("                padding: 4px 8px;\n");
      out.write("                width: 25px;\n");
      out.write("                height: 25px;\n");
      out.write("                background: linear-gradient(to left, white, #16ff88);\n");
      out.write("                background-color: #16ff88;\n");
      out.write("                animation: ring1 1s infinite;\n");
      out.write("                animation-delay: 1s;\n");
      out.write("            }\n");
      out.write("            \n");
      out.write("            .ring0{\n");
      out.write("                background-color: white;\n");
      out.write("                color: white;\n");
      out.write("            }\n");
      out.write("            .ring1{\n");
      out.write("      \n");
      out.write("            }\n");
      out.write("            .ring2{\n");
      out.write("                background-color: #16dc88;\n");
      out.write("            }\n");
      out.write("            .ring3{\n");
      out.write("                background-color: #16dc4b;\n");
      out.write("            }\n");
      out.write("            .ring4{\n");
      out.write("                background-color:#16c816;\n");
      out.write("            }\n");
      out.write("            .ring5{\n");
      out.write("                background-color: #16ab00;\n");
      out.write("            }\n");
      out.write("            \n");
      out.write("            @keyframes ring1{\n");
      out.write("                0%{\n");
      out.write("                    transform: translateX(100%);\n");
      out.write("                }\n");
      out.write("                100%{\n");
      out.write("                    transform: translateX(-100%);\n");
      out.write("                }\n");
      out.write("            }\n");
      out.write("            \n");
      out.write("        </style>\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"Css/botones.css\">\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <form>\n");
      out.write("            <div>\n");
      out.write("                <div></div>\n");
      out.write("                <table border=\"2\">\n");
      out.write("                    <tr>\n");
      out.write("                        <td id=\"prueba1\" class=\"ring5\">0</td>\n");
      out.write("                        <td id=\"prueba2\" class=\"ring5\">1</td>\n");
      out.write("                        <td id=\"prueba3\" class=\"ring5\">2</td>\n");
      out.write("                        <td class=\"ring5\">3</td>\n");
      out.write("                        <td class=\"ring5\">4</td>\n");
      out.write("                        <td class=\"ring5\">5</td>\n");
      out.write("                        <td class=\"ring5\">6</td>\n");
      out.write("                        <td class=\"ring5\">7</td>\n");
      out.write("                        <td class=\"ring5\">8</td>\n");
      out.write("                        <td class=\"ring5\">9</td>\n");
      out.write("                    </tr>\n");
      out.write("                    <tr>\n");
      out.write("                        <td class=\"ring5\">0</td>\n");
      out.write("                        <td class=\"ring4\">1</td>\n");
      out.write("                        <td class=\"ring4\">2</td>\n");
      out.write("                        <td class=\"ring4\">3</td>\n");
      out.write("                        <td class=\"ring4\">4</td>\n");
      out.write("                        <td class=\"ring4\">5</td>\n");
      out.write("                        <td class=\"ring4\">6</td>\n");
      out.write("                        <td class=\"ring4\">7</td>\n");
      out.write("                        <td class=\"ring4\">8</td>\n");
      out.write("                        <td class=\"ring5\">9</td>\n");
      out.write("                    </tr>\n");
      out.write("                    <tr>\n");
      out.write("                        <td class=\"ring5\">0</td>\n");
      out.write("                        <td class=\"ring4\">1</td>\n");
      out.write("                        <td class=\"ring3\">2</td>\n");
      out.write("                        <td class=\"ring3\">3</td>\n");
      out.write("                        <td class=\"ring3\">4</td>\n");
      out.write("                        <td class=\"ring3\">5</td>\n");
      out.write("                        <td class=\"ring3\">6</td>\n");
      out.write("                        <td class=\"ring3\">7</td>\n");
      out.write("                        <td class=\"ring4\">8</td>\n");
      out.write("                        <td class=\"ring5\">9</td>\n");
      out.write("                    </tr>\n");
      out.write("                    <tr>\n");
      out.write("                        <td class=\"ring5\">0</td>\n");
      out.write("                        <td class=\"ring4\">1</td>\n");
      out.write("                        <td class=\"ring3\">2</td>\n");
      out.write("                        <td class=\"ring2\">3</td>\n");
      out.write("                        <td class=\"ring2\">4</td>\n");
      out.write("                        <td class=\"ring2\">5</td>\n");
      out.write("                        <td class=\"ring2\">6</td>\n");
      out.write("                        <td class=\"ring3\">7</td>\n");
      out.write("                        <td class=\"ring4\">8</td>\n");
      out.write("                        <td class=\"ring5\">9</td>\n");
      out.write("                    </tr>\n");
      out.write("                    <tr>\n");
      out.write("                        <td class=\"ring5\">0</td>\n");
      out.write("                        <td class=\"ring4\">1</td>\n");
      out.write("                        <td class=\"ring3\">2</td>\n");
      out.write("                        <td class=\"ring2\">3</td>\n");
      out.write("                        <td class=\"ring1\">4</td>\n");
      out.write("                        <td class=\"ring1\">5</td>\n");
      out.write("                        <td class=\"ring2\">6</td>\n");
      out.write("                        <td class=\"ring3\">7</td>\n");
      out.write("                        <td class=\"ring4\">8</td>\n");
      out.write("                        <td class=\"ring5\">9</td>\n");
      out.write("                    </tr>\n");
      out.write("                    <tr>\n");
      out.write("                        <td class=\"ring5\">0</td>\n");
      out.write("                        <td class=\"ring4\">1</td>\n");
      out.write("                        <td class=\"ring3\">2</td>\n");
      out.write("                        <td class=\"ring2\">3</td>\n");
      out.write("                        <td class=\"ring1\"><span>4</span></td>\n");
      out.write("                        <td class=\"ring1\">5</td>\n");
      out.write("                        <td class=\"ring2\">6</td>\n");
      out.write("                        <td class=\"ring3\">7</td>\n");
      out.write("                        <td class=\"ring4\">8</td>\n");
      out.write("                        <td class=\"ring5\">9</td>\n");
      out.write("                    </tr>\n");
      out.write("                    <tr>\n");
      out.write("                        <td class=\"ring5\">0</td>\n");
      out.write("                        <td class=\"ring4\">1</td>\n");
      out.write("                        <td class=\"ring3\">2</td>\n");
      out.write("                        <td class=\"ring2\">3</td>\n");
      out.write("                        <td class=\"ring2\">4</td>\n");
      out.write("                        <td class=\"ring2\">5</td>\n");
      out.write("                        <td class=\"ring2\">6</td>\n");
      out.write("                        <td class=\"ring3\">7</td>\n");
      out.write("                        <td class=\"ring4\">8</td>\n");
      out.write("                        <td class=\"ring5\">9</td>\n");
      out.write("                    </tr>\n");
      out.write("                    <tr>\n");
      out.write("                        <td class=\"ring5\">0</td>\n");
      out.write("                        <td class=\"ring4\">1</td>\n");
      out.write("                        <td class=\"ring3\">2</td>\n");
      out.write("                        <td class=\"ring3\">3</td>\n");
      out.write("                        <td class=\"ring3\">4</td>\n");
      out.write("                        <td class=\"ring3\">5</td>\n");
      out.write("                        <td class=\"ring3\">6</td>\n");
      out.write("                        <td class=\"ring3\">7</td>\n");
      out.write("                        <td class=\"ring4\">8</td>\n");
      out.write("                        <td class=\"ring5\">9</td>\n");
      out.write("                    </tr>\n");
      out.write("                    <tr>\n");
      out.write("                        <td class=\"ring5\">0</td>\n");
      out.write("                        <td class=\"ring4\">1</td>\n");
      out.write("                        <td class=\"ring4\">2</td>\n");
      out.write("                        <td class=\"ring4\">3</td>\n");
      out.write("                        <td class=\"ring4\">4</td>\n");
      out.write("                        <td class=\"ring4\">5</td>\n");
      out.write("                        <td class=\"ring4\">6</td>\n");
      out.write("                        <td class=\"ring4\">7</td>\n");
      out.write("                        <td class=\"ring4\">8</td>\n");
      out.write("                        <td class=\"ring5\">9</td>\n");
      out.write("                    </tr>\n");
      out.write("                    <tr>\n");
      out.write("                        <td class=\"ring5\">0</td>\n");
      out.write("                        <td class=\"ring5\">1</td>\n");
      out.write("                        <td class=\"ring5\">2</td>\n");
      out.write("                        <td class=\"ring5\">3</td>\n");
      out.write("                        <td class=\"ring5\">4</td>\n");
      out.write("                        <td class=\"ring5\">5</td>\n");
      out.write("                        <td class=\"ring5\">6</td>\n");
      out.write("                        <td class=\"ring5\">7</td>\n");
      out.write("                        <td class=\"ring5\">8</td>\n");
      out.write("                        <td class=\"ring5\">9</td>\n");
      out.write("                    </tr>               \n");
      out.write("                </table>\n");
      out.write("                \n");
      out.write("                <div class=\"botones\">\n");
      out.write("                    <button class=\"btn1\" type=\"button\" onclick=\"\">\n");
      out.write("                        <span></span>\n");
      out.write("                        <span></span>\n");
      out.write("                        <span></span>\n");
      out.write("                        <span></span>\n");
      out.write("                        Forward\n");
      out.write("                    </button>\n");
      out.write("                    <button class=\"btn2\"  type=\"button\" onclick=\"backward()\">\n");
      out.write("                        <span></span>\n");
      out.write("                        <span></span>\n");
      out.write("                        <span></span>\n");
      out.write("                        <span></span>\n");
      out.write("                        Backward\n");
      out.write("                    </button>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </form>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
