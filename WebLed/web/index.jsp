<%-- 
    Document   : newjsp
    Created on : 04-29-2019, 10:31:18 AM
    Author     : luis.castillousam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" type="text/css" href="Css/botones.css">
    </head>
    <body>
        <form>
            <div>
                <div></div>
                <table border="2">
                    <tr>
                        <td class="ring5 parpadea5">0</td>
                        <td class="ring5 parpadea5">1</td>
                        <td class="ring5 parpadea5">2</td>
                        <td class="ring5 parpadea5">3</td>
                        <td class="ring5 parpadea5">4</td>
                        <td class="ring5 parpadea5">5</td>
                        <td class="ring5 parpadea5">6</td>
                        <td class="ring5 parpadea5">7</td>
                        <td class="ring5 parpadea5">8</td>
                        <td class="ring5 parpadea5">9</td>
                    </tr>
                    <tr>
                        <td class="ring5 parpadea5">0</td>
                        <td class="ring4 parpadea4">1</td>
                        <td class="ring4 parpadea4">2</td>
                        <td class="ring4 parpadea4">3</td>
                        <td class="ring4 parpadea4">4</td>
                        <td class="ring4 parpadea4">5</td>
                        <td class="ring4 parpadea4">6</td>
                        <td class="ring4 parpadea4">7</td>
                        <td class="ring4 parpadea4">8</td>
                        <td class="ring5 parpadea5">9</td>
                    </tr>
                    <tr>
                        <td class="ring5 parpadea5">0</td>
                        <td class="ring4 parpadea4">1</td>
                        <td class="ring3 parpadea3">2</td>
                        <td class="ring3 parpadea3">3</td>
                        <td class="ring3 parpadea3">4</td>
                        <td class="ring3 parpadea3">5</td>
                        <td class="ring3 parpadea3">6</td>
                        <td class="ring3 parpadea3">7</td>
                        <td class="ring4 parpadea4">8</td>
                        <td class="ring5 parpadea5">9</td>
                    </tr>
                    <tr>
                        <td class="ring5 parpadea5">0</td>
                        <td class="ring4 parpadea4">1</td>
                        <td class="ring3 parpadea3">2</td>
                        <td class="ring2 parpadea2">3</td>
                        <td class="ring2 parpadea2">4</td>
                        <td class="ring2 parpadea2">5</td>
                        <td class="ring2 parpadea2">6</td>
                        <td class="ring3 parpadea3">7</td>
                        <td class="ring4 parpadea4">8</td>
                        <td class="ring5 parpadea5">9</td>
                    </tr>
                    <tr>
                        <td class="ring5 parpadea5">0</td>
                        <td class="ring4 parpadea4">1</td>
                        <td class="ring3 parpadea3">2</td>
                        <td class="ring2 parpadea2">3</td>
                        <td class="ring1 parpadea1">4</td>
                        <td class="ring1 parpadea1">5</td>
                        <td class="ring2 parpadea2">6</td>
                        <td class="ring3 parpadea3">7</td>
                        <td class="ring4 parpadea4">8</td>
                        <td class="ring5 parpadea5">9</td>
                    </tr>
                        <tr>
                        <td class="ring5 parpadea5">0</td>
                        <td class="ring4 parpadea4">1</td>
                        <td class="ring3 parpadea3">2</td>
                        <td class="ring2 parpadea2">3</td>
                        <td class="ring1 parpadea1">4</td>
                        <td class="ring1 parpadea1">5</td>
                        <td class="ring2 parpadea2">6</td>
                        <td class="ring3 parpadea3">7</td>
                        <td class="ring4 parpadea4">8</td>
                        <td class="ring5 parpadea5">9</td>
                    </tr>
                    <tr>
                        <td class="ring5 parpadea5">0</td>
                        <td class="ring4 parpadea4">1</td>
                        <td class="ring3 parpadea3">2</td>
                        <td class="ring2 parpadea2">3</td>
                        <td class="ring2 parpadea2">4</td>
                        <td class="ring2 parpadea2">5</td>
                        <td class="ring2 parpadea2">6</td>
                        <td class="ring3 parpadea3">7</td>
                        <td class="ring4 parpadea4">8</td>
                        <td class="ring5 parpadea5">9</td>
                    </tr>
                    <tr>
                        <td class="ring5 parpadea5">0</td>
                        <td class="ring4 parpadea4">1</td>
                        <td class="ring3 parpadea3">2</td>
                        <td class="ring3 parpadea3">3</td>
                        <td class="ring3 parpadea3">4</td>
                        <td class="ring3 parpadea3">5</td>
                        <td class="ring3 parpadea3">6</td>
                        <td class="ring3 parpadea3">7</td>
                        <td class="ring4 parpadea4">8</td>
                        <td class="ring5 parpadea5">9</td>
                    </tr>
                    <tr>
                        <td class="ring5 parpadea5">0</td>
                        <td class="ring4 parpadea4">1</td>
                        <td class="ring4 parpadea4">2</td>
                        <td class="ring4 parpadea4">3</td>
                        <td class="ring4 parpadea4">4</td>
                        <td class="ring4 parpadea4">5</td>
                        <td class="ring4 parpadea4">6</td>
                        <td class="ring4 parpadea4">7</td>
                        <td class="ring4 parpadea4">8</td>
                        <td class="ring5 parpadea5">9</td>
                    </tr>
                    <tr>
                        <td class="ring5 parpadea5">0</td>
                        <td class="ring5 parpadea5">1</td>
                        <td class="ring5 parpadea5">2</td>
                        <td class="ring5 parpadea5">3</td>
                        <td class="ring5 parpadea5">4</td>
                        <td class="ring5 parpadea5">5</td>
                        <td class="ring5 parpadea5">6</td>
                        <td class="ring5 parpadea5">7</td>
                        <td class="ring5 parpadea5">8</td>
                        <td class="ring5 parpadea5">9</td>
                    </tr>         
                </table>
                
                <div class="botones">
                    <button class="btn1" type="button" onclick="color1()">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        Forward
                    </button>
                    <button class="btn2"  type="button" onclick="backward()">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        Backward
                    </button>
                </div>
            </div>
        </form>
    </body>
</html>
